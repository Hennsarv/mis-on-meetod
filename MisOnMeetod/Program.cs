﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnMeetod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("see meetod Main teeb midagi");
            
            Console.WriteLine(AnnaArv("vasakpoolne") * AnnaArv("parempoolne"));
        }

        static int AnnaArv(string milline)
        {
            int arv;

            Console.Write($"Anna {milline}: ");
            while (!int.TryParse(Console.ReadLine(), out arv))
            {
                Console.WriteLine("See pole arv");
                Console.Write($"Anna {milline}: ");
            }
            return arv;
           
        }
    }
}
